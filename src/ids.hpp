#ifndef _SRC_IDS_HPP
#define _SRC_IDS_HPP

#include <algorithm>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>
#include <variant>

/**
 * @def POSITION_MACRO
 * @brief Print colorized position in project (filename, loc)
 */
#define POSITION_MACRO "\x1B[4m\x1B[34m" << __FILE__ << "\x1B[0m"  \
    << ":" << "\x1B[1m\x1B[31m" << __func__ << "()\x1B[0m"         \
    << ":\x1B[32m" << __LINE__ << "\x1B[0m:"

namespace st2se {

    /**
     * value format enum
     */
    enum class ValueFormat {
        KEY_VALUE,          /**< value is format key and value*/
        VALUE,              /**< value is format value*/
        EMPTY               /**< value is empty*/
    };

    /**
     * value type enum
     */
    enum class ValueType {
        POINTER,            /**< value type is pointer*/
        INTEGER,            /**< value type is integer */
        STRING,             /**< value type is string*/
        CONSTANT,           /**< value type is constant*/
        ARRAY,              /**< value type is array*/
        STRUCTURE,          /**< value type is structure*/
        BITFIELD,           /**< value type is bitfield*/
        CLUSTERS,           /**< value type is cluster*/
        EMPTY               /**< value is empty */
    };

    enum class NumberValues {
        EMPTY,              /**< Argument does not operate with multiple values or value is empty*/
        VALUE,              /**< Argument contains only one value */
        RANGE,              /**< Argument contains values as range */
        VALUES,             /**< Argument contains discrete values in vector */
        VALUES_INTERVAL     /**< Argument contains range and discrete values */
    };

    /**
     * Argument structure
     * This structure holds information about argument
     */
    struct Argument {

        using Value = std::variant<long, std::string>; /**< Alias for value type */
        ValueFormat valueFormat {ValueFormat::EMPTY};           /**< value type */
        ValueType valueType {ValueType::EMPTY};                 /**< value format */
        NumberValues numbervalues {NumberValues::EMPTY};        /**< nubmer of values */
        std::string key {""};                                   /**< key before value, typically key=value */
        Value value {""};                                       /**< this variable stores value of argument. */
        Value value_b {""};                                     /**< this variable stores value of argument. This variable is used only after opitmization phase */
        
        /**
         * Constructor
         * default constructor
         */
        Argument();
        /**
         * Constructor
         * explicit constructor
         * @param _fmt an value format
         * @param _type an value type
         * @param _vec arguments that are on next position after this argument
         */
        Argument(ValueFormat _fmt, ValueType _type);
        /**
         * Constructor
         * explicit constructor
         * @param _fmt an value format
         * @param _type an value type
         * @param _key an key before value
         * @param _value an value of argument
         * @param _vec arguments that are on next position after this argument
         */
        Argument(ValueFormat &_fmt, ValueType &_type, std::string &_key, std::variant<long, std::string> _value);
        /**
         * Print class variables.
         * This member will print all inner variables of this argument and much more.
         */
        void print();
    };

    using Arguments = std::vector<Argument>;
    using Cluster = std::vector<Arguments>;
    using Clusters = std::vector<Cluster>;

    /**
     * Data structure that holds information about syscall
     */
    struct Syscall {
        std::string name {""}; /**< syscall name */
        int return_code {0}; /**< return code */
        std::string other {""}; /**< content parsed after return code */
        unsigned arg_num {0}; /**< number of arguments in the syscall*/
        std::vector<Arguments> next {}; /**< first arguments */
        bool clustered = false; /**< points if the content is clustered or not*/
        Clusters clusters {};

        /**
         * Print syscall.
         */
        void print();

        /**
         * Print syscall if it's clustered.
         * This method is called from print()
         */
        void printClustered();
    };

    /**
     * Object that implements whole IDS
     */
    class Ids {
        using Sc_map = std::map<std::string, Syscall>;
      public:
        Sc_map data {}; /***< here are located syscalls */
        /**
         * Syscall inserter into IDS
         * @param name syscall name
         * @param sc syscall content
         * @return true if initialized false otherwise
         */
        bool insert(const std::string &name, Syscall &sc);
        
        /**
         * Print syscall
         */
        void printSyscall();
        
        /**
         * Print IDS.
         */
        void print();
    };

    /**
     * Convert argument to string
     * @param arg an argument
     * @return string value represented as string
     */
    std::string arg2str(const Argument &arg);
    /**
     * Operator overload
     * This operator implements comparison of two arguments
     */
    bool operator== (const st2se::Argument &lhs, const st2se::Argument &rhs);
    /**
     * Operator overload
     * This operator implements less operator for two arguments
     */
    bool operator< (const st2se::Argument &lhs, const st2se::Argument &rhs);
    /**
     * Operator overload
     * This operator implements printing of IDS to output stream
     */
    std::ostream &operator<< (std::ostream &os, const st2se::Ids &a);
    /**
     * Operator overload
     * This operator implements printing of value type enumeration to output stream
     */
    std::ostream &operator<< (std::ostream &os, const st2se::ValueType &a);
    /**
     * Operator overload
     * This operator implements printing of value format enumeration to output stream
     */
    std::ostream &operator<< (std::ostream &os, const st2se::ValueFormat &a);

} // namespace st2se

#endif
