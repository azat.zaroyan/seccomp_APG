#include "algorithm.hpp"

namespace st2se {

    std::vector<Argument> Algorithm::getArguemntsFromPos(
        const Syscall& sc,
        unsigned arg_pos )
    {
        std::vector<Argument> v;
        size_t syscall_count = sc.next.size();
        for (size_t index = 0; index < syscall_count; ++ index) {
            if (sc.next[index][arg_pos].valueType != ValueType::INTEGER) return v;
            v.push_back(sc.next[index][arg_pos]);
        }

        return v;
    }

    unsigned Algorithm::getArgMaxCount(const Syscall& sc)
    {
        unsigned size = 0;
        for (auto args : sc.next) {
            if (args.size() > size) {
                size = args.size();
            }
        }

        return size;
    }
} // namespace st2se
