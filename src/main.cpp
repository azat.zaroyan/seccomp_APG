#include <iostream>
#include <memory>

#include "tao/pegtl.hpp"

#include "argparse.hpp"
#include "ids.hpp"
#include "optimizer.hpp"
#include "states.hpp"
#include "straceparser.hpp"
#include "algorithm_strict.hpp"
#include "algorithm_clustered.hpp"
#include "generator.hpp"

template<typename Algo>
// FIXME change name of the function
int callOptimize(st2se::Ids &in, st2se::Ids &out) {
    int ret_val {0}; // return value

    // create optimizer and pass pointer to Algorithm in a constructor
    //st2se::Optimizer opti(std::make_unique<Algo>().get);
    Algo algo;
    st2se::Optimizer opti(&algo);
    try {
        opti.optimize(in, out);
    }
    catch (std::runtime_error &e) {
        std::cerr << e.what() << std::endl
            << "Exiting with error ..." << std::endl;
        ret_val = 1;
    }

    return ret_val;
}

template<typename Gen>
// FIXME change name of the function
int callGenerator(Params &params, st2se::Ids &out) {

    st2se::Generator gen;

    std::unique_ptr<st2se::Output> cpp = std::make_unique<Gen>();

    // initialize and configure generator
    gen.initialize(cpp.get());
    gen.configure(params);

    gen.generate(out);

    // remove and dealloc output generator
    gen.removeOutput();

    return 0;
}

int checkParams(Params &params) {
    if (params.debug) {
        std::cout << params << std::endl;
    }

    // print usage when no algorithm was emitted
    if (!params.strict && !params.weak && !params.clustered) {
        params.printHelp();
        exit(0);
    }

    return 0;
}

int main(int argc, char *argv[]) {

    int ret_val {0};

    Params params(argc, argv);

    checkParams(params);

    st2se::Ids in {};
    st2se::Ids out {};
    st2se::States states {};
    st2se::StraceParser o(in);

    // run grammar analysis
    if (params.analysis != 0) {

        std::cout << "Analyzing grammar ..." << std::endl;

        const std::size_t issues = o.AnalyzeGrammar();

        std::cout << "Found " << issues << " issues." << std::endl;

        return ret_val;
    }

    // parse files
    for (const auto &fn : params.file_names) {
        o.parse(fn, in, params, states);
    }
    // optimize IDS
    switch (params.algorithm) {
    case algo::STRICT:
        ret_val = callOptimize<st2se::Algo_strict>(in, out);
        break;

    case algo::CLUSTERED:
        ret_val = callOptimize<st2se::Algo_clustered>(in, out);
        break;

    default:
        std::cerr << "Algorithm not used" << std::endl;
        break;
    }

    if (ret_val) {
        return ret_val;
    }
    
    // Call generator for specific language
    switch (params.language) {
    default:
        callGenerator<st2se::outputCPP>(params, out);

    }

    return ret_val;

}
