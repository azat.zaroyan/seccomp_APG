#ifndef SRC_ALGORITHM_CLUSTERED_HPP
#define SRC_ALGORITHM_CLUSTERED_HPP

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_set>
#include <variant>
#include <vector>

#include "algorithm.hpp"
#include "ids.hpp"

namespace st2se {

    using Bitfield = std::vector<std::string>;

    /**
     * DBSCAN algorithm implementation
     */
    class Algo_clustered : public Algorithm {
        /**
         * Process Syscall method
         * This member works only with one syscall
         * @param sc an Syscall used as input
         * @param out an IDS used as output
         * @return bool true on success false on error
         */
        void processSyscall(const Syscall &sc, Ids &out);
        /**
         * Create cluster
         * This member will process arguments from input into cluster on the output
         * @param in an argument vector used as input
         * @param out an argument vector used as cluster output
         * @return unsigned number of members in cluster
         */
        unsigned cluster(std::vector<Argument> &in, Cluster &out);
        /**
         * Return pair of two arguments which are the closest together
         * Method return two arguments which has clossest distance to each other
         * @param in an argument vector used as input
         * @return pair of nearest arguments
         */
        std::pair<Argument, Argument> smallestDst(Arguments &in);
        /**
         * Computes distance between two arguments
         * This member will compute the distance between two arguments
         * @param left an argument
         * @param right an argument
         * @return double distance between the two arguments
         */
        double distance(Argument &left, Argument &right);

        /**
         * deletes arguments from vector
         * This member will remove argument from vector
         * @param arg an argument which will be deleted
         * @param vec vector from which will be deleted
         * @return bool true on success false on error
         */
        bool removeItem(Argument &arg, Arguments &vec);
        /**
         * returns closest items to argument with treshold
         * @param arg an argument
         * @param vec an vector of arguments
         * @param double eps between the two arguments
         * @return items in vector
         */
        std::vector<Argument> closestItemsTo(Argument &arg, Arguments &vec, double eps);
        /**
         * move cluster from x to y
         * This member will move cluster
         * @param cluster an cluster
         * @param out an space
         * @return bool true on success false on error
         */
        bool moveCluster(Arguments &cluster, Cluster &out);
        /**
         * This member will move first argument from vec1 to vec2
         * @param to an vector
         * @param from an vector
         * @return bool true on success false on error
         */
        bool moveFirstItem(Arguments &to, Arguments &from);
        /**
         * Optimization method
         * This member overrides the optimize memver in Algorithm class.
         * In this member is located the core implementation of DBSCAN algorithm
         * @param in an IDS used as input
         * @param out an IDS used as output
         * @return bool true on success false on error
         */
        bool optimize(Ids &in, Ids &out) override;
    };

    /**
     * Computes bitfield distance
     * @param a an bitfiled
     * @param b an bitfiled
     * @return int distance
     */
    int bitfieldDistance(Bitfield &a, Bitfield &b);
    /**
     * Convert any argument to bitfield
     * @param a an argument
     * @return Bitfield bitfield
     */
    Bitfield convert2bitfield(const Argument &in);

} // namespace st2se

#endif
