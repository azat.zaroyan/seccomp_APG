#include "outputCPP.hpp"

namespace st2se {

    inline void outputCPP::openFiles() {
        template_file_begin.open(template_file_b_path, std::ios::in);
        template_file_end.open(template_file_e_path, std::ios::in);
        template_file_thread.open(template_file_t_path, std::ios::in);
        output_source.open(output_source_path, std::ios_base::out | std::ios_base::trunc);

        if (!template_file_begin.is_open()) {
            throw (std::runtime_error("template_file_begin is not open"));
        }

        if (!template_file_end.is_open()) {
            throw (std::runtime_error("template_file_end is not open"));
        }

        if (!template_file_thread.is_open()) {
            throw (std::runtime_error("template_file_thread is not open"));
        }

        if (!output_source.is_open()) {
            throw (std::runtime_error("output_source is not open"));
        }
    }

    inline void outputCPP::closeFiles() {
        template_file_begin.close();
        template_file_end.close();
        template_file_thread.close();
        output_source.close();
    }

    inline void outputCPP::writeFirstPart() {
        std::string line;

        while (getline(template_file_begin, line)) {
            output_source << line << std::endl;
        }
    }

    inline void outputCPP::writeFuncProlog() {
        output_source << fmt::format("{0}int {1}(){2}\n", opt_indent, funcName, '{');
        output_source << fmt::format("{0}{1}scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);\n", indent, opt_indent);
        output_source << fmt::format("{0}{1}int ret = 0;\n", indent, opt_indent);
    }

    inline void outputCPP::writeFuncEpilog() {
        output_source << fmt::format("{0}{1}\n", opt_indent, '}');
    }


    inline void outputCPP::writeLastPart() {
        std::string line;

        // print the end of the  template.c
        while (getline(template_file_end, line)) {
            output_source << line << std::endl;
        }
    }

    inline void outputCPP::writeThreadPart() {
        std::string line;

        while (getline(template_file_thread, line)) {
            output_source << line << std::endl;
        }
    }

    inline void outputCPP::setOutput(std::string o) {
        output_source_path = o;
    }


    void outputCPP::generate(Ids &ids) {

        openFiles();

        if (genProlog) {
            opt_indent = "    ";
            writeFirstPart();
        }

        writeFuncProlog();

        if (genThreading) {
            writeThreadPart();
        }

        // ids.print();

        for (const auto &item : ids.data) {
            generateScRules(item);
        }

        if (genProlog) {
            writeLastPart();
        }
        else {
            output_source << fmt::format("{0}{1}return ret;\n", opt_indent, indent);
            writeFuncEpilog();
        }


        closeFiles();

    }


    void outputCPP::generateScRules(std::pair<std::string, Syscall> sc_pair) {

        Syscall &sc = sc_pair.second;

        if (!sc.next.empty()) {

            for (auto &item : sc.next) {
                batch.clear();
                unsigned pos {0};
                rule_start = getRuleProlog(sc);
                generateRules(item, sc, pos);
            }

        }

        printRules();

    }

    void outputCPP::generateRules(Arguments &arg, Syscall& sc, unsigned pos) {
        std::string condition;
        for (auto &item : arg) {
            Cluster cluster;
            if(!sc.clusters.empty()) {
                cluster = sc.clusters[pos];
            }
            condition = getCondition(item, pos++, cluster);

            if (condition.compare("")) {
                batch.push_back(condition);
            }
        }

        writeRule();
        
        if (condition.compare("")) {
            batch.pop_back();
        }
    }

    std::string outputCPP::getCondition(Argument &arg, unsigned pos, Cluster& cluster) {

        if (arg.valueFormat == ValueFormat::EMPTY) {
            return std::string {""};
        }

        std::string ret = fmt::format(",\n{0}{0}{1}SCMP_A{2}(", indent, opt_indent, pos);

        switch (arg.valueType) {
        case ValueType::INTEGER:
            if(!cluster.empty()) {
                Argument first, last;
                bool it_is_not_noise = false;
                for(auto args: cluster) {
                    if(std::find(args.begin(), args.end(), arg) != args.end()) {
                        first = args.front();
                        last = args.back();
                        if (args.size() > 1) {
                            it_is_not_noise = true;
                        }
                        break;
                    }
                }
                if (it_is_not_noise) {
                    ret +=  fmt::format("SCMP_CMP_GE, {0})", arg2str(last));
                    ret +=  fmt::format(",\n{0}{0}{1}SCMP_A{2}(", indent, opt_indent, pos);
                    ret +=  fmt::format("SCMP_CMP_LE, {0})", arg2str(first));
                    break;
                } else {
                    ret += fmt::format("SCMP_CMP_EQ, {0})", arg2str(arg));
                    break;
                }
            } else {
                ret += fmt::format("SCMP_CMP_EQ, {0})", arg2str(arg));
                break;
            }
        case ValueType::CONSTANT:
            ret += fmt::format("SCMP_CMP_EQ, {0})", arg2str(arg));
            break;

        case ValueType::BITFIELD:
            ret += fmt::format("SCMP_CMP_MASKED_EQ, {0}, {0})", arg2str(arg));
            break;

        default:
            return std::string {""};
            break;
        }

        return ret;
    }

    std::string outputCPP::getRuleProlog(Syscall &sc) {
        std::string ret {""};

        ret = fmt::format(
                "{0}{1}ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS({2})",
                indent,
                opt_indent,
                sc.name
            );

        return ret;
    }

    void outputCPP::writeRule() {

        std::string buffer {""};

        buffer += fmt::format("{0}, {1}", rule_start, batch.size());

        if (!batch.empty()) {
            for (auto &item : batch) {
                buffer += fmt::format("{}", item);
            }

            buffer += fmt::format("\n{0}{1});\n", opt_indent, indent);
        }
        else {
            buffer += fmt::format(");\n", indent);
        }

        ready2Print.push_back(buffer);
    }

    void outputCPP::printRules() {

        std::sort(ready2Print.begin(), ready2Print.end());
        auto end = std::unique(ready2Print.begin(), ready2Print.end());
        ready2Print.erase(end, ready2Print.end());


        for (auto &item : ready2Print) {
            output_source << item;
        }

        output_source.flush();

        ready2Print.clear();

    }


} // namespace st2se
