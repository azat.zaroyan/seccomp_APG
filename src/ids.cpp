#include "ids.hpp"

template<class T> struct always_false : std::false_type {};

namespace st2se {

    std::ostream &operator<< (std::ostream &os, const st2se::Ids &a) {
        for (const auto &item : a.data) {
            os << item.first
                << " has "
                << item.second.next.size()
                << " arguments"
                << std::endl;
        }

        return os << std::endl << std::endl;
    }

    bool operator<(const st2se::Argument &lhs, const st2se::Argument &rhs) {

        if (lhs.value < rhs.value) {
            return true;
        }

        return false;
    }


    bool operator==(const st2se::Argument &lhs, const st2se::Argument &rhs) {
        bool b_val = false;

        if (auto lval = std::get_if<long>(&lhs.value))
            if (auto rval = std::get_if<long>(&rhs.value))
                if (*lval == *rval) {
                    b_val = true;
                }

        if (auto lval = std::get_if<std::string>(&lhs.value))
            if (auto rval = std::get_if<std::string>(&rhs.value))
                if (!lval->compare(*rval)) {
                    b_val = true;
                }

        return lhs.valueType == rhs.valueType &&
            lhs.valueFormat == rhs.valueFormat &&
            !lhs.key.compare(rhs.key) &&
            b_val;
    }

    std::ostream &operator<< (std::ostream &os, const st2se::ValueFormat &a) {
        // TODO transformt this into switch case statement
        if (a == st2se::ValueFormat::KEY_VALUE) {
            return os << "KEY_VALUE";
        }

        if (a == st2se::ValueFormat::VALUE) {
            return os << "VALUE";
        }

        if (a == st2se::ValueFormat::EMPTY) {
            return os << "EMPTY";
        }

        return os << "UNDEF";
    }

    std::ostream &operator<< (std::ostream &os, const st2se::ValueType &a) {
        // TODO transformt this into switch case statement
        if (a == st2se::ValueType::INTEGER) {
            return os << "INTEGER";
        }

        if (a == st2se::ValueType::STRING) {
            return os << "STRING";
        }

        if (a == st2se::ValueType::CONSTANT) {
            return os << "CONSTANT";
        }

        if (a == st2se::ValueType::POINTER) {
            return os << "POINTER";
        }

        if (a == st2se::ValueType::ARRAY) {
            return os << "ARRAY";
        }

        if (a == st2se::ValueType::STRUCTURE) {
            return os << "STRUCTURE";
        }

        if (a == st2se::ValueType::BITFIELD) {
            return os << "BITFIELD";
        }

        if (a == st2se::ValueType::CLUSTERS) {
            return os << "CLUSTERS";
        }

        if (a == st2se::ValueType::EMPTY) {
            return os << "EMPTY";
        }

        return os << "UNDEF";
    }

    Argument::Argument(ValueFormat _fmt, ValueType _type)
        : valueFormat(_fmt), valueType(_type){
    }

    Argument::Argument(ValueFormat &_fmt, ValueType &_type, std::string &_key,
        std::variant<long, std::string> _value)
        : valueFormat(_fmt), valueType(_type), key(_key), value(_value) {
    }

    Argument::Argument():
        valueFormat(ValueFormat::EMPTY),
        valueType(ValueType::EMPTY),
        key(""),
        value(""){
    }

    void Argument::print() {

        std::cout << "\t" << arg2str(*this) << "\b" << std::endl;
    }

    bool Ids::insert(const std::string &name, Syscall &sc) {

        Syscall &root_sc = data[name];

        if (root_sc.next.empty()) {
            root_sc.name = sc.name;
            root_sc.return_code = sc.return_code;
            root_sc.other = sc.other;
            root_sc.arg_num = sc.arg_num;
        }
        root_sc.next.push_back(sc.next.back());
        return true;
    }

    void Ids::print() {
        std::cout << *this << std::endl;

        for (auto item : data) {
            item.second.print();
        }
    }

    void Syscall::print() {
        std::cout << name << ":" << std::endl ; 
        if (next.empty()) {
            std::cout << "No args" << std::endl;
        } else {
            for (auto args : next) {
                for (auto arg : args) {
                    std::cout << "  '" << arg2str(arg) << "',";
                }
                std::cout << std::endl;
            }
        }
        if (clustered) {
            printClustered();
        }
    }

    void Syscall::printClustered() {
        if (next.empty()) {
            std::cout << "No args" << std::endl;
        }
        else {
            int pos = 0;
            for (auto cluster : clusters) {
                std::cout << "Clusters of " << pos++ << " position: " << std::endl;
                int cluster_number = 0;
                for (auto args : cluster) {
                    std::cout << "\t" << "cluster " << cluster_number++ << ":" << std::endl;
                    std::cout << "\t";
                    for (auto arg : args) {
                        std::cout << "  '" << arg2str(arg) << "',";
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
        }
    }

    // move this function to utilities
    std::string arg2str(const Argument &arg) {
        if (std::holds_alternative<long>(arg.value))
            return std::to_string(std::get<long>(arg.value));
        else
            return std::get<std::string>(arg.value);
    }
} // namespace st2se
