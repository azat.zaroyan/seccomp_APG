#include "states.hpp"

template<class T> struct always_false : std::false_type {};

namespace st2se {
    void States::push_parsed_val(Argument &arg) {
        parsed_val.push_back(arg);
    }

    void States::pop_parsed_val() {
        parsed_val.pop_back();
    }

    bool States::process_val(Ids &ids) {

        Syscall s = getSyscall();

        ids.insert(get_name(), s);

        // s.print();

        return true;
    }

    void States::set_val_format(const ValueFormat &fmt) {
        arg_format = fmt;
    }

    void States::set_val_type(const ValueType &fmt) {
        last_arg_type = fmt;
    }

    void States::set_ret_val(const std::string &str) {
        std::stringstream buf(str);
        buf >> return_val;
    }

    void States::set_name(const std::string &str) {
        name = str;
    }

    void States::set_bitfields(const bool &b) {
        bitfields = b;
    }

    int States::get_ret_val() {
        return return_val;
    }

    bool &States::get_bitfields() {
        return bitfields;
    }

    std::string &States::get_name() {
        return name;
    }

    ValueFormat &States::get_val_format() {
        return arg_format;
    }

    ValueType &States::get_val_type() {
        return last_arg_type;
    }

    void States::clear() {
        parsed_val.clear();
        arg_num = 0;
        bitfields = false;
    }

    std::string States::argsStr() {
        std::string str {" "};

        for (auto &w : parsed_val) {
            std::string tmp;

            if (w.valueType == ValueType::INTEGER) {
                    tmp = std::to_string(std::get<long>(w.value));
            }
            else if (w.valueType == ValueType::STRING) {
                tmp = std::get<std::string>(w.value);
            }
            else {
                std::cerr << "Error: Variant is empty";
                tmp = "";
            }

            str.append(tmp + ",\n ");
        }

        return str;
    }

    Syscall States::getSyscall() {
        Syscall s;

        if (parsed_val.empty()) {
            // std::cerr << "Error: " << this->get_name() << ": Parsed val is empty." << std::endl;

            Arguments a;

            s.next.emplace_back(a);
            s.arg_num = this->arg_num;
            s.return_code = this->return_val;
            s.name = this->get_name();

        }
        else {
            Arguments a;
            for( auto item: parsed_val ){
                a.push_back(item);
            }
            s.next.push_back(a);
            s.arg_num = this->arg_num;
            s.return_code = this->return_val;
            s.name = this->get_name();
        }


        return s;

    }

} // namespace st2se
