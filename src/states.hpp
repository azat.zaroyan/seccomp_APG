#ifndef _SRC_STATES_HPP
#define _SRC_STATES_HPP

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>

#if __cplusplus < 201703L
    #include <mpark/variant.hpp>
#else
    #include <variant>
#endif

#include <vector>

#include "ids.hpp"


namespace st2se {
    class States {
        // TODO
        // - define a copy constructor
        // - copy assignment operator
        // - move constructor
        // - move assignment operator

        std::vector<st2se::Argument> parsed_val {}; /**< all parsed values */

        ValueFormat arg_format {ValueFormat::EMPTY}; /**< value format*/
        ValueType last_arg_type {ValueType::EMPTY}; /**< value type*/
        long long return_val {0}; /**< return value */
        std::string name {}; /**< name of syscall */
        bool bitfields {false}; /**< bool that indicates if was bitfields recognized */

      public:
        unsigned arg_num {0}; /**< number of parsed arguments */

        #if __cplusplus < 201703L
        mpark::variant<long, std::string> value; /**< value */
        #else
        std::variant<long, std::string> value; /**< value */
        #endif

        void push_parsed_val(Argument &arg);
        void pop_parsed_val();
        /**
         * Move parsed syscall to ids
         * @param [out] ids IDS
         * @return bool true on success, false otherwise
         */
        bool process_val(Ids &ids);

        /**
         * Setter
         * set value format
         * @param fmt value format
         */
        void set_val_format(const ValueFormat &fmt);
        /**
         * Setter
         * set value type
         * @param fmt value type
         */
        void set_val_type(const ValueType &fmt);
        /**
         * Setter
         * set return value
         * @param str set return value
         */
        void set_ret_val(const std::string &str);
        /**
         * Setter
         * set syscall name
         * @param str syscall name
         */
        void set_name(const std::string &str);
        /**
         * Setter
         * set bitfield switch
         * @param b bitfield recognized
         */
        void set_bitfields(const bool &b);

        /**
         * Getter
         * get bitfields
         * @return bool if bitfield was recognized
         */
        bool &get_bitfields();
        /**
         * Getter
         * get return value
         * @return return value
         */
        int get_ret_val();
        /**
         * Getter
         * get syscall name
         * @return string name
         */
        std::string &get_name();
        /**
         * Getter
         * get value format
         * @return value format
         */
        ValueFormat &get_val_format();
        /**
         * Getter
         * get value type
         * @return value type
         */
        ValueType &get_val_type();

        /**
         * Converts arguemnts to string
         * @return argument in string
         */
        std::string argsStr();
        /**
         * Get syscall
         * @return syscall
         */
        Syscall getSyscall();
        /**
         * Clear states
         */
        void clear();

        /**
         * Constructor
         * default constructor
         */
        States() = default;
        /**
         * destructor
         * default destructor
         */
        ~States() = default;

    };
} // namespace st2se

#endif
