/*
 * Generated seccomp template with initialized filter using seccomp_APG.
 * link with -lseccomp
 */

#ifdef __cplusplus
    #define NULL nullptr
    #include <cstdio>
#else
    #include <stdio.h>
#endif

#include <seccomp.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cerrno>
#include <unistd.h>
#include <asm/unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <signal.h>


    int setup_seccomp_whitelist(){
        scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
        int ret = 0;
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, R_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, W_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, X_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_EQ, 23)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_EQ, 33)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_EQ, 34)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_EQ, 35)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 110)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 110)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(eventfd2), 2,
            SCMP_A0(SCMP_CMP_EQ, 0),
            SCMP_A1(SCMP_CMP_MASKED_EQ, EFD_CLOEXEC|EFD_NONBLOCK, EFD_CLOEXEC|EFD_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 1,
            SCMP_A0(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A1(SCMP_CMP_EQ, F_GETFL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A1(SCMP_CMP_EQ, F_SETFL),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_NONBLOCK, O_RDWR|O_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 23)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 25)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 33)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 34)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_EQ, 35)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 1)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 107)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 11)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 112)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 16)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 2)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 3)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 30)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 38)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 43)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 49)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 5)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 53)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 58)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 64)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 69)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 75)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 81)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 83)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 87)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 93)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 99)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAKE_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 1)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAKE_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 2147483647)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getdents64), 3,
            SCMP_A0(SCMP_CMP_EQ, 35),
            SCMP_A1(SCMP_CMP_EQ, 35),
            SCMP_A2(SCMP_CMP_EQ, 32768)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getegid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(geteuid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getpid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(gettid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getuid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, SEEK_CUR)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lstat), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(madvise), 2,
            SCMP_A1(SCMP_CMP_EQ, 8368128),
            SCMP_A2(SCMP_CMP_EQ, MADV_DONTNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 28672),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 134217728),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE),
            SCMP_A4(SCMP_CMP_EQ, -1),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 22328),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_EQ, -1),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 32768),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_EQ, 24),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 6648),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_EQ, 25),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 77154),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_EQ, 24),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 79672),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_EQ, 24),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 135168),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 4096),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 67108864)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 77154)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 8392704)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_EQ, O_RDONLY)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_CLOEXEC, O_RDONLY|O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY, O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_EQ, 1),
            SCMP_A2(SCMP_CMP_EQ, 25000)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(prctl), 1,
            SCMP_A0(SCMP_CMP_EQ, PR_SET_NAME)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 8192)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 16)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 4096)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 8192)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 832)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 33),
            SCMP_A2(SCMP_CMP_EQ, 1024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 33),
            SCMP_A2(SCMP_CMP_EQ, 4096)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 35),
            SCMP_A2(SCMP_CMP_EQ, 1024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 35),
            SCMP_A2(SCMP_CMP_EQ, 4096)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(readlink), 1,
            SCMP_A2(SCMP_CMP_EQ, 4095)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sched_setattr), 2,
            SCMP_A0(SCMP_CMP_EQ, 9482),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendmsg), 2,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, MSG_NOSIGNAL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 19),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 24),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 6),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_EQ, 23),
            SCMP_A2(SCMP_CMP_EQ, 7),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(set_robust_list), 1,
            SCMP_A1(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(socket), 3,
            SCMP_A0(SCMP_CMP_EQ, AF_UNIX),
            SCMP_A1(SCMP_CMP_MASKED_EQ, SOCK_STREAM|SOCK_CLOEXEC, SOCK_STREAM|SOCK_CLOEXEC),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(socket), 3,
            SCMP_A0(SCMP_CMP_EQ, AF_UNIX),
            SCMP_A1(SCMP_CMP_MASKED_EQ, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(stat), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(statfs), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 3),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 4),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 6),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        //-------------------------------------------------------------------------
        int filter_fd;
        filter_fd = open("/tmp/seccomp_filter.bpf", O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
        if (filter_fd == -1) {
             ret = -errno;
             goto out;
        }

        ret = seccomp_export_bpf(ctx, filter_fd);
        if (ret < 0) {
             close(filter_fd);
             goto out;
        }
        close(filter_fd);


        if (seccomp_load(ctx) != 0) {
            ret = 2;
            goto out;
        }

        return ret;

    out:
        seccomp_release(ctx);
        return ret;
    }

int main()
{
    setup_seccomp_whitelist();

    // Put your code below
    return 0;

}
