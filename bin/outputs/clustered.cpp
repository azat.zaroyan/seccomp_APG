/*
 * Generated seccomp template with initialized filter using seccomp_APG.
 * link with -lseccomp
 */

#ifdef __cplusplus
    #define NULL nullptr
    #include <cstdio>
#else
    #include <stdio.h>
#endif

#include <seccomp.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cerrno>
#include <unistd.h>
#include <asm/unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <signal.h>


    int setup_seccomp_whitelist(){
        scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
        int ret = 0;
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, F_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, R_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_EQ, X_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(access), 1,
            SCMP_A1(SCMP_CMP_MASKED_EQ, R_OK|X_OK, R_OK|X_OK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(arch_prctl), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(arch_prctl), 1,
            SCMP_A0(SCMP_CMP_EQ, ARCH_SET_FS)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(brk), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(chmod), 1,
            SCMP_A1(SCMP_CMP_EQ, 700)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(clone), 1,
            SCMP_A1(SCMP_CMP_MASKED_EQ, CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(clone), 1,
            SCMP_A1(SCMP_CMP_MASKED_EQ, CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 1,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 40)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_EQ, 29),
            SCMP_A2(SCMP_CMP_EQ, 110)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_GE, 20),
            SCMP_A2(SCMP_CMP_LE, 35)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_EQ, 110)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(connect), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_GE, 20),
            SCMP_A2(SCMP_CMP_LE, 35)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(eventfd2), 2,
            SCMP_A0(SCMP_CMP_EQ, 0),
            SCMP_A1(SCMP_CMP_MASKED_EQ, EFD_CLOEXEC|EFD_NONBLOCK, EFD_CLOEXEC|EFD_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(execve), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 10128),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 10336),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 11272),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 145768),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 15560),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 16312),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 16784),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 1968),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 20008),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 22280),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 22960),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 2360),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 24856),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 29512),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 3008),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 3288),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 3368),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 3520),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 4224),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 44064),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 5360),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 55112),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 62456),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 6560),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 77200),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 87624),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_EQ, 9208),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 1688),
            SCMP_A2(SCMP_CMP_LE, 1728),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 1784),
            SCMP_A2(SCMP_CMP_LE, 1824),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 1864),
            SCMP_A2(SCMP_CMP_LE, 1904),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 2120),
            SCMP_A2(SCMP_CMP_LE, 2144),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 2416),
            SCMP_A2(SCMP_CMP_LE, 2448),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 3200),
            SCMP_A2(SCMP_CMP_LE, 3224),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fadvise64), 4,
            SCMP_A0(SCMP_CMP_EQ, 24),
            SCMP_A1(SCMP_CMP_EQ, 0),
            SCMP_A2(SCMP_CMP_GE, 4792),
            SCMP_A2(SCMP_CMP_LE, 4808),
            SCMP_A3(SCMP_CMP_EQ, POSIX_FADV_WILLNEED)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_GETFD)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_GETFL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_SETLKW)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 10),
            SCMP_A1(SCMP_CMP_EQ, F_GETFL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 10),
            SCMP_A1(SCMP_CMP_EQ, F_GETLK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 10),
            SCMP_A1(SCMP_CMP_EQ, F_SETLK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_SETFD),
            SCMP_A2(SCMP_CMP_EQ, FD_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_SETFL),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NONBLOCK, O_RDONLY|O_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_SETFL),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_NONBLOCK, O_RDWR|O_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_EQ, F_SETFL),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_WRONLY|O_NONBLOCK, O_WRONLY|O_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fcntl), 3,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 10),
            SCMP_A1(SCMP_CMP_EQ, F_SETFL),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_NONBLOCK, O_RDWR|O_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 1,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 35)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstatfs), 1,
            SCMP_A0(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(ftruncate), 2,
            SCMP_A0(SCMP_CMP_EQ, 28),
            SCMP_A1(SCMP_CMP_EQ, 67108864)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 1,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_LOCK_PI_PRIVATE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 1,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_UNLOCK_PI_PRIVATE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAIT_PRIVATE),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 2)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAKE_PRIVATE),
            SCMP_A2(SCMP_CMP_EQ, 2147483647)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(futex), 2,
            SCMP_A1(SCMP_CMP_EQ, FUTEX_WAKE_PRIVATE),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 2)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getdents64), 3,
            SCMP_A0(SCMP_CMP_EQ, 3),
            SCMP_A1(SCMP_CMP_EQ, 3),
            SCMP_A2(SCMP_CMP_EQ, 32768)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getdents64), 3,
            SCMP_A0(SCMP_CMP_GE, 22),
            SCMP_A0(SCMP_CMP_LE, 33),
            SCMP_A1(SCMP_CMP_GE, 22),
            SCMP_A1(SCMP_CMP_LE, 33),
            SCMP_A2(SCMP_CMP_EQ, 32768)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getegid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(geteuid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getgid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getpeername), 1,
            SCMP_A0(SCMP_CMP_EQ, 2)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getpeername), 1,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getpid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getrandom), 1,
            SCMP_A1(SCMP_CMP_GE, 8),
            SCMP_A1(SCMP_CMP_LE, 16)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getrandom), 2,
            SCMP_A1(SCMP_CMP_GE, 8),
            SCMP_A1(SCMP_CMP_LE, 16),
            SCMP_A2(SCMP_CMP_EQ, GRND_NONBLOCK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getresgid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getresuid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getsockname), 1,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(gettid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getuid), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(inotify_add_watch), 2,
            SCMP_A0(SCMP_CMP_EQ, 22),
            SCMP_A2(SCMP_CMP_MASKED_EQ, IN_MODIFY|IN_ATTRIB|IN_CLOSE_WRITE|IN_MOVED_FROM|IN_MOVED_TO|IN_CREATE|IN_DELETE|IN_DELETE_SELF|IN_MOVE_SELF|IN_UNMOUNT|IN_ONLYDIR, IN_MODIFY|IN_ATTRIB|IN_CLOSE_WRITE|IN_MOVED_FROM|IN_MOVED_TO|IN_CREATE|IN_DELETE|IN_DELETE_SELF|IN_MOVE_SELF|IN_UNMOUNT|IN_ONLYDIR)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(inotify_init1), 1,
            SCMP_A0(SCMP_CMP_EQ, IN_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(inotify_rm_watch), 2,
            SCMP_A0(SCMP_CMP_EQ, 22),
            SCMP_A1(SCMP_CMP_GE, 15),
            SCMP_A1(SCMP_CMP_LE, 19)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(inotify_rm_watch), 2,
            SCMP_A0(SCMP_CMP_EQ, 22),
            SCMP_A1(SCMP_CMP_GE, 2),
            SCMP_A1(SCMP_CMP_LE, 6)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(ioctl), 2,
            SCMP_A0(SCMP_CMP_EQ, 2),
            SCMP_A1(SCMP_CMP_EQ, TCGETS)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 102400),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 106496),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 110592),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 114688),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 118784),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 12288),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 122880),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 126976),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 131072),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 135168),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 139264),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 143360),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 16384),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 20480),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 24576),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 28672),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 32768),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 36864),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 40960),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 45056),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 49152),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 53248),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 57344),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 61440),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 65536),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 69632),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 73728),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 77824),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 81920),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 86016),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 90112),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 94208),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_EQ, 98304),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_GE, -734),
            SCMP_A1(SCMP_CMP_LE, 0),
            SCMP_A2(SCMP_CMP_EQ, SEEK_CUR)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lseek), 3,
            SCMP_A0(SCMP_CMP_GE, 24),
            SCMP_A0(SCMP_CMP_LE, 32),
            SCMP_A1(SCMP_CMP_GE, -734),
            SCMP_A1(SCMP_CMP_LE, 0),
            SCMP_A2(SCMP_CMP_EQ, SEEK_SET)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(lstat), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(memfd_create), 1,
            SCMP_A1(SCMP_CMP_EQ, MFD_ALLOW_SEALING)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mkdir), 1,
            SCMP_A1(SCMP_CMP_GE, 700),
            SCMP_A1(SCMP_CMP_LE, 777)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 102400),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 106496),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1126400),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 114688),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 118784),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 118784),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 118784),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1220608),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 12288),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 12288),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 12288),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 122880),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 122880),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 126976),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 126976),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1273856),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 135168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 139264),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1466368),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 151552),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 151552),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1540096),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 155648),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 1560576),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 159744),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 16384),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 16384),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 16384),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 163840),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 167936),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 167936),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 172032),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 176128),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 188416),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 196608),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 200704),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 204800),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 212992),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 221184),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 233472),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 245760),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 253952),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 28037120),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 2838528),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 28672),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 28672),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 28672),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 294912),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 299008),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 303104),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 319488),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 319488),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 323584),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 331776),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 335872),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 344064),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 352256),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 356352),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 3600384),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 380928),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 380928),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 3817472),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 397312),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 4096),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 4096),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 413696),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 417792),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 45056),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 45056),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 45056),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 479232),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 4808704),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 487424),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 524288),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 528384),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 53248),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 53248),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 540672),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 544768),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 548864),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 565248),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 569344),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 57344),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 57344),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 57344),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 598016),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 606208),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 61440),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 61440),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 61440),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 626688),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 65536),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 65536),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 65536),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 684032),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 69632),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 69632),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 69632),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 729088),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 77824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 77824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 77824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 802816),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 806912),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 815104),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 81920),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 843776),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 847872),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 86016),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 86016),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 90112),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 90112),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 917504),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 937984),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 94208),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 94208),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 98304),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 98304),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_EQ, 987136),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 131072),
            SCMP_A1(SCMP_CMP_LE, 131096),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 131072),
            SCMP_A1(SCMP_CMP_LE, 131096),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 184320),
            SCMP_A1(SCMP_CMP_LE, 184336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 20480),
            SCMP_A1(SCMP_CMP_LE, 20512),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 20480),
            SCMP_A1(SCMP_CMP_LE, 20512),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 20480),
            SCMP_A1(SCMP_CMP_LE, 20512),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 24576),
            SCMP_A1(SCMP_CMP_LE, 24632),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 24576),
            SCMP_A1(SCMP_CMP_LE, 24632),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 24576),
            SCMP_A1(SCMP_CMP_LE, 24632),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 249856),
            SCMP_A1(SCMP_CMP_LE, 249888),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 32768),
            SCMP_A1(SCMP_CMP_LE, 32824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 32768),
            SCMP_A1(SCMP_CMP_LE, 32824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 32768),
            SCMP_A1(SCMP_CMP_LE, 32824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 36864),
            SCMP_A1(SCMP_CMP_LE, 36880),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 36864),
            SCMP_A1(SCMP_CMP_LE, 36880),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 36864),
            SCMP_A1(SCMP_CMP_LE, 36880),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 40960),
            SCMP_A1(SCMP_CMP_LE, 40976),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 40960),
            SCMP_A1(SCMP_CMP_LE, 40976),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 40960),
            SCMP_A1(SCMP_CMP_LE, 40976),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 49152),
            SCMP_A1(SCMP_CMP_LE, 49168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 49152),
            SCMP_A1(SCMP_CMP_LE, 49168),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 73728),
            SCMP_A1(SCMP_CMP_LE, 73744),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 4,
            SCMP_A1(SCMP_CMP_GE, 73728),
            SCMP_A1(SCMP_CMP_LE, 73744),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 10128),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 10240),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 102480),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 102768),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 10336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1056),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1064160),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1068784),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 107592),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 108760),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 109920),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 110776),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 11272),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 113280),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 114904),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1171400),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 117336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1191536),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1199),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1216056),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1216968),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 122080),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 12288),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1257376),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1264),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1267200),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 128952),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1297720),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 131168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1338608),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 13432),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 135168),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 135520),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 135568),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1368336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 13920),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 139624),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 139872),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 140408),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 141248),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 145768),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 15560),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1579272),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 160088),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 16312),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 163992),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 164104),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 16680),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 16784),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 168616),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 169144),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 172032),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 172472),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 174600),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 180408),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1806584),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 182536),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 184480),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 193416),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1966880),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 196696),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1968),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1972224),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 1987008),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 20008),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 200954),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 2037344),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 20600),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 2117648),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 2134632),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 2139336),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_EXEC, PROT_READ|PROT_EXEC),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 221968),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 222216),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 22280),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 225296),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 22960),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 233592),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 234272),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 2360),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 238576),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 24744),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 24856),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 252760),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 253768),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 254568),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 258744),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 259920),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 266240),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 266608),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 27002),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 28049424),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 28800),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 288392),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 28936),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 29512),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3008),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 310416),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 312128),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3138280),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3194208),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 320176),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 327184),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 328400),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 32944),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 333612),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_EQ, 33),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 333612),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_NORESERVE, MAP_PRIVATE|MAP_NORESERVE),
            SCMP_A4(SCMP_CMP_EQ, 33),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 336168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3368),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 340712),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 344136),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3520),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 353824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 353824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_NORESERVE, MAP_PRIVATE|MAP_NORESERVE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 354088),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 363086),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 368),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 36928),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 3696),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 390504),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 392072),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 39904),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 41120),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 41376),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 41456),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 4224),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 43152),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 4344),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 44064),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 45240),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 45336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 4544),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 456),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 4655),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 46632),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 467208),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 48160),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 4896),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 49544),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 49608),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 50856),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 508960),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 515552),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 519776),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 529272),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 53352),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 5360),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 536040),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 55112),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 5704176),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 57464),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 577536),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 58344),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 590632),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 62456),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 637704),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 6560),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 65832),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 6664),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 66728),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 67108864),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_SHARED|MAP_NORESERVE, MAP_SHARED|MAP_NORESERVE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 6744),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 6784),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 680264),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 688192),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 692936),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 696400),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 69808),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 69864),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 70056),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 713968),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 714360),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 724168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 73792),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 76840),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 77154),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 77200),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 7776),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 778256),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 784),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 796880),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 8061344),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 8192),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS, MAP_PRIVATE|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 82768),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 8392704),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 84152),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 87624),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 88),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 904640),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 9208),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 9553624),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 98472),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 98624),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 131072),
            SCMP_A1(SCMP_CMP_LE, 131096),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 16424),
            SCMP_A1(SCMP_CMP_LE, 16455),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 16424),
            SCMP_A1(SCMP_CMP_LE, 16455),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 16488),
            SCMP_A1(SCMP_CMP_LE, 16552),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 1688),
            SCMP_A1(SCMP_CMP_LE, 1824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 1688),
            SCMP_A1(SCMP_CMP_LE, 1824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 184320),
            SCMP_A1(SCMP_CMP_LE, 184336),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 1864),
            SCMP_A1(SCMP_CMP_LE, 1904),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 20480),
            SCMP_A1(SCMP_CMP_LE, 20512),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 20744),
            SCMP_A1(SCMP_CMP_LE, 20752),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 20864),
            SCMP_A1(SCMP_CMP_LE, 20896),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 20864),
            SCMP_A1(SCMP_CMP_LE, 20896),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 2120),
            SCMP_A1(SCMP_CMP_LE, 2144),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 2416),
            SCMP_A1(SCMP_CMP_LE, 2448),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 24576),
            SCMP_A1(SCMP_CMP_LE, 24632),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 249856),
            SCMP_A1(SCMP_CMP_LE, 249888),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 3200),
            SCMP_A1(SCMP_CMP_LE, 3224),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 3272),
            SCMP_A1(SCMP_CMP_LE, 3328),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_PRIVATE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 3272),
            SCMP_A1(SCMP_CMP_LE, 3328),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 3272),
            SCMP_A1(SCMP_CMP_LE, 3328),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 32768),
            SCMP_A1(SCMP_CMP_LE, 32824),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 32768),
            SCMP_A1(SCMP_CMP_LE, 32824),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 36864),
            SCMP_A1(SCMP_CMP_LE, 36880),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 40960),
            SCMP_A1(SCMP_CMP_LE, 40976),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 4792),
            SCMP_A1(SCMP_CMP_LE, 4808),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_EQ, MAP_SHARED),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 49152),
            SCMP_A1(SCMP_CMP_LE, 49168),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 712),
            SCMP_A1(SCMP_CMP_LE, 744),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_GE, 73728),
            SCMP_A1(SCMP_CMP_LE, 73744),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_DENYWRITE, MAP_PRIVATE|MAP_DENYWRITE),
            SCMP_A4(SCMP_CMP_GE, -1),
            SCMP_A4(SCMP_CMP_LE, 28),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 1019904),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 1114112),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 1134592),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 1179648),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 1290240),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 2093056),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 2113536),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 2277376),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 524288),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 561152),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 581632),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 8388608),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 1490944),
            SCMP_A1(SCMP_CMP_LE, 1499136),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 212992),
            SCMP_A1(SCMP_CMP_LE, 225280),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 212992),
            SCMP_A1(SCMP_CMP_LE, 225280),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 278528),
            SCMP_A1(SCMP_CMP_LE, 290816),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 4096),
            SCMP_A1(SCMP_CMP_LE, 184320),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 4096),
            SCMP_A1(SCMP_CMP_LE, 184320),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 450560),
            SCMP_A1(SCMP_CMP_LE, 466944),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 700416),
            SCMP_A1(SCMP_CMP_LE, 712704),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 798720),
            SCMP_A1(SCMP_CMP_LE, 802816),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_GE, 798720),
            SCMP_A1(SCMP_CMP_LE, 802816),
            SCMP_A2(SCMP_CMP_EQ, PROT_READ)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mremap), 3,
            SCMP_A1(SCMP_CMP_GE, 135168),
            SCMP_A1(SCMP_CMP_LE, 1052672),
            SCMP_A2(SCMP_CMP_GE, 266240),
            SCMP_A2(SCMP_CMP_LE, 1052672),
            SCMP_A3(SCMP_CMP_EQ, MREMAP_MAYMOVE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 145768)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 172032)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 32768)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 577536)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 77154)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_GE, 1199),
            SCMP_A1(SCMP_CMP_LE, 4655)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_EQ, O_RDONLY)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_CLOEXEC, O_RDONLY|O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NOCTTY|O_CLOEXEC, O_RDONLY|O_NOCTTY|O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NOCTTY|O_NOFOLLOW|O_CLOEXEC, O_RDONLY|O_NOCTTY|O_NOFOLLOW|O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NOFOLLOW|O_CLOEXEC, O_RDONLY|O_NOFOLLOW|O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 2,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY, O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 3,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_CREAT, O_RDWR|O_CREAT),
            SCMP_A3(SCMP_CMP_EQ, 600)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 3,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_CREAT|O_NOCTTY|O_CLOEXEC, O_RDWR|O_CREAT|O_NOCTTY|O_CLOEXEC),
            SCMP_A3(SCMP_CMP_EQ, 644)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 3,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_RDWR|O_CREAT|O_NOFOLLOW|O_CLOEXEC, O_RDWR|O_CREAT|O_NOFOLLOW|O_CLOEXEC),
            SCMP_A3(SCMP_CMP_EQ, 644)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 3,
            SCMP_A0(SCMP_CMP_EQ, AT_FDCWD),
            SCMP_A2(SCMP_CMP_MASKED_EQ, O_WRONLY|O_CREAT|O_APPEND, O_WRONLY|O_CREAT|O_APPEND),
            SCMP_A3(SCMP_CMP_EQ, 660)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pipe2), 1,
            SCMP_A1(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pipe2), 1,
            SCMP_A1(SCMP_CMP_EQ, O_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 11804)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 14358)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 17683)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 17761)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 18837)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 25000)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 2641)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 406)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 422)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 4472)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 5321)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 60000)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 6319)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 7197)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_EQ, 8334)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, -1),
            SCMP_A2(SCMP_CMP_LE, 137)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 143),
            SCMP_A2(SCMP_CMP_LE, 203)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 19991),
            SCMP_A2(SCMP_CMP_LE, 19993)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 210),
            SCMP_A2(SCMP_CMP_LE, 211)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 216),
            SCMP_A2(SCMP_CMP_LE, 217)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 222),
            SCMP_A2(SCMP_CMP_LE, 225)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 230),
            SCMP_A2(SCMP_CMP_LE, 233)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 239),
            SCMP_A2(SCMP_CMP_LE, 241)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 247),
            SCMP_A2(SCMP_CMP_LE, 249)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 254),
            SCMP_A2(SCMP_CMP_LE, 275)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 280),
            SCMP_A2(SCMP_CMP_LE, 283)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 288),
            SCMP_A2(SCMP_CMP_LE, 290)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 297),
            SCMP_A2(SCMP_CMP_LE, 300)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 306),
            SCMP_A2(SCMP_CMP_LE, 308)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 313),
            SCMP_A2(SCMP_CMP_LE, 317)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 322),
            SCMP_A2(SCMP_CMP_LE, 324)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 330),
            SCMP_A2(SCMP_CMP_LE, 333)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 338),
            SCMP_A2(SCMP_CMP_LE, 340)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 347),
            SCMP_A2(SCMP_CMP_LE, 350)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 356),
            SCMP_A2(SCMP_CMP_LE, 357)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 363),
            SCMP_A2(SCMP_CMP_LE, 366)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 372),
            SCMP_A2(SCMP_CMP_LE, 374)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 379),
            SCMP_A2(SCMP_CMP_LE, 384)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 389),
            SCMP_A2(SCMP_CMP_LE, 401)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 414),
            SCMP_A2(SCMP_CMP_LE, 417)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 430),
            SCMP_A2(SCMP_CMP_LE, 434)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 439),
            SCMP_A2(SCMP_CMP_LE, 450)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 455),
            SCMP_A2(SCMP_CMP_LE, 467)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_GE, 1),
            SCMP_A1(SCMP_CMP_LE, 3),
            SCMP_A2(SCMP_CMP_GE, 472),
            SCMP_A2(SCMP_CMP_LE, 500)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 24),
            SCMP_A3(SCMP_CMP_GE, 10972),
            SCMP_A3(SCMP_CMP_LE, 11033)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 24),
            SCMP_A3(SCMP_CMP_GE, 12044),
            SCMP_A3(SCMP_CMP_LE, 12271)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 4),
            SCMP_A3(SCMP_CMP_EQ, 624)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 4),
            SCMP_A3(SCMP_CMP_GE, 0),
            SCMP_A3(SCMP_CMP_LE, 64)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 4),
            SCMP_A3(SCMP_CMP_GE, 10972),
            SCMP_A3(SCMP_CMP_LE, 11033)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 4),
            SCMP_A3(SCMP_CMP_GE, 12044),
            SCMP_A3(SCMP_CMP_LE, 12271)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_EQ, 4),
            SCMP_A3(SCMP_CMP_GE, 264),
            SCMP_A3(SCMP_CMP_LE, 356)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_GE, 32),
            SCMP_A2(SCMP_CMP_LE, 40),
            SCMP_A3(SCMP_CMP_GE, 10972),
            SCMP_A3(SCMP_CMP_LE, 11033)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_EQ, 31),
            SCMP_A2(SCMP_CMP_GE, 32),
            SCMP_A2(SCMP_CMP_LE, 40),
            SCMP_A3(SCMP_CMP_GE, 12044),
            SCMP_A3(SCMP_CMP_LE, 12271)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 100),
            SCMP_A3(SCMP_CMP_GE, 0),
            SCMP_A3(SCMP_CMP_LE, 64)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1007616)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 102400)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1036288)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1060864)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 106496)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1085440)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1118208)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1134592)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1167360)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 118784)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1191936)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1220608)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1241088)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 126976)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1290240)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1343488)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1372160)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1376256)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1380352)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1384448)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1409024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1429504)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1458176)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1495040)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 151552)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1515520)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1527808)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1556480)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1634304)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 163840)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1654784)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1658880)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1662976)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1667072)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1671168)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1679360)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1699840)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1720320)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1736704)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1814528)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1826816)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1863680)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1880064)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1900544)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1916928)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1929216)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1945600)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 196608)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1970176)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 1990656)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2002944)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2052096)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2068480)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2072576)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2076672)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2121728)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2125824)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2129920)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2154496)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2166784)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2195456)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2215936)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2248704)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2256896)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2281472)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2326528)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2330624)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 233472)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2347008)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2359296)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2387968)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2408448)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 241664)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2433024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2437120)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2469888)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2514944)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2535424)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2609152)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2633728)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2670592)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2674688)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2715648)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2744320)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2760704)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2781184)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2805760)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2822144)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2842624)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2867200)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2891776)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 2916352)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 294912)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 3002368)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 3039232)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 3055616)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 360448)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 380928)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 385024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 389120)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 393216)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 442368)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 487424)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 491520)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 495616)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 520192)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 569344)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 573440)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 577536)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 585728)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 614400)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 630784)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 651264)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 671744)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 679936)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 700416)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 724992)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 753664)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 782336)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 811008)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 8192)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 819200)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 868352)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 872448)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 884736)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 909312)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 933888)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 958464)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 991232)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_GE, 0),
            SCMP_A3(SCMP_CMP_LE, 64)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 68),
            SCMP_A3(SCMP_CMP_GE, 824),
            SCMP_A3(SCMP_CMP_LE, 880)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_EQ, 784),
            SCMP_A3(SCMP_CMP_GE, 0),
            SCMP_A3(SCMP_CMP_LE, 64)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pread64), 3,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 17),
            SCMP_A2(SCMP_CMP_GE, 32),
            SCMP_A2(SCMP_CMP_LE, 40),
            SCMP_A3(SCMP_CMP_GE, 824),
            SCMP_A3(SCMP_CMP_LE, 880)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(prlimit64), 2,
            SCMP_A0(SCMP_CMP_EQ, 0),
            SCMP_A1(SCMP_CMP_EQ, RLIMIT_STACK)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(pwrite64), 3,
            SCMP_A0(SCMP_CMP_EQ, 11),
            SCMP_A2(SCMP_CMP_EQ, 1),
            SCMP_A3(SCMP_CMP_EQ, 1)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 1024)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 2048)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 312)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 336)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 36)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 4096)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 633)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 65536)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 8192)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_EQ, 832)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_GE, 104),
            SCMP_A2(SCMP_CMP_LE, 120)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_GE, 152),
            SCMP_A2(SCMP_CMP_LE, 168)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_GE, 248),
            SCMP_A2(SCMP_CMP_LE, 270)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_GE, 3),
            SCMP_A0(SCMP_CMP_LE, 39),
            SCMP_A2(SCMP_CMP_GE, 4),
            SCMP_A2(SCMP_CMP_LE, 16)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(readlink), 1,
            SCMP_A2(SCMP_CMP_EQ, 4095)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(readlink), 1,
            SCMP_A2(SCMP_CMP_GE, 99),
            SCMP_A2(SCMP_CMP_LE, 256)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_EQ, 14604),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_EQ, 1588),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_EQ, 7352),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_GE, 1332),
            SCMP_A2(SCMP_CMP_LE, 1348),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_GE, 8),
            SCMP_A2(SCMP_CMP_LE, 16),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvfrom), 3,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_EQ, 4096),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvmsg), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvmsg), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_EQ, MSG_CMSG_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigaction), 2,
            SCMP_A0(SCMP_CMP_EQ, 13),
            SCMP_A3(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigaction), 2,
            SCMP_A0(SCMP_CMP_EQ, 32),
            SCMP_A3(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigprocmask), 2,
            SCMP_A0(SCMP_CMP_EQ, SIG_SETMASK),
            SCMP_A3(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigprocmask), 2,
            SCMP_A0(SCMP_CMP_EQ, SIG_UNBLOCK),
            SCMP_A3(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sched_getattr), 3,
            SCMP_A0(SCMP_CMP_EQ, 9475),
            SCMP_A2(SCMP_CMP_EQ, 56),
            SCMP_A3(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sched_setattr), 2,
            SCMP_A0(SCMP_CMP_EQ, 9475),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendmsg), 2,
            SCMP_A0(SCMP_CMP_EQ, 20),
            SCMP_A2(SCMP_CMP_EQ, MSG_NOSIGNAL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendmsg), 2,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_EQ, MSG_NOSIGNAL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 20),
            SCMP_A0(SCMP_CMP_LE, 27),
            SCMP_A2(SCMP_CMP_EQ, 1),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 20),
            SCMP_A0(SCMP_CMP_LE, 27),
            SCMP_A2(SCMP_CMP_EQ, 19),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 20),
            SCMP_A0(SCMP_CMP_LE, 27),
            SCMP_A2(SCMP_CMP_EQ, 24),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 20),
            SCMP_A0(SCMP_CMP_LE, 27),
            SCMP_A2(SCMP_CMP_GE, 6),
            SCMP_A2(SCMP_CMP_LE, 7),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_EQ, 19),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_EQ, 24),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendto), 4,
            SCMP_A0(SCMP_CMP_GE, 5),
            SCMP_A0(SCMP_CMP_LE, 7),
            SCMP_A2(SCMP_CMP_GE, 6),
            SCMP_A2(SCMP_CMP_LE, 7),
            SCMP_A3(SCMP_CMP_EQ, MSG_NOSIGNAL),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(set_robust_list), 1,
            SCMP_A1(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(set_tid_address), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(setsockopt), 4,
            SCMP_A0(SCMP_CMP_EQ, 29),
            SCMP_A1(SCMP_CMP_EQ, SOL_SOCKET),
            SCMP_A2(SCMP_CMP_EQ, SO_PRIORITY),
            SCMP_A4(SCMP_CMP_EQ, 4)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(shmat), 2,
            SCMP_A0(SCMP_CMP_GE, 53),
            SCMP_A0(SCMP_CMP_LE, 58),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(shmctl), 2,
            SCMP_A0(SCMP_CMP_GE, 53),
            SCMP_A0(SCMP_CMP_LE, 58),
            SCMP_A1(SCMP_CMP_EQ, IPC_RMID)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(shmdt), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(shutdown), 2,
            SCMP_A0(SCMP_CMP_EQ, 20),
            SCMP_A1(SCMP_CMP_EQ, SHUT_RDWR)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(socket), 3,
            SCMP_A0(SCMP_CMP_EQ, AF_UNIX),
            SCMP_A1(SCMP_CMP_MASKED_EQ, SOCK_STREAM|SOCK_CLOEXEC, SOCK_STREAM|SOCK_CLOEXEC),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(stat), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(statfs), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(umask), 1,
            SCMP_A0(SCMP_CMP_GE, 2),
            SCMP_A0(SCMP_CMP_LE, 77)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(uname), 0);
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(wait4), 2,
            SCMP_A0(SCMP_CMP_EQ, 9492),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 33),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 27),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_GE, 2),
            SCMP_A0(SCMP_CMP_LE, 11),
            SCMP_A2(SCMP_CMP_EQ, 171)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_GE, 2),
            SCMP_A0(SCMP_CMP_LE, 11),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(writev), 2,
            SCMP_A0(SCMP_CMP_GE, 19),
            SCMP_A0(SCMP_CMP_LE, 20),
            SCMP_A2(SCMP_CMP_GE, 1),
            SCMP_A2(SCMP_CMP_LE, 6)
        );
        //-------------------------------------------------------------------------
        int filter_fd;
        filter_fd = open("/tmp/seccomp_filter.bpf", O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
        if (filter_fd == -1) {
             ret = -errno;
             goto out;
        }

        ret = seccomp_export_bpf(ctx, filter_fd);
        if (ret < 0) {
             close(filter_fd);
             goto out;
        }
        close(filter_fd);


        if (seccomp_load(ctx) != 0) {
            ret = 2;
            goto out;
        }

        return ret;

    out:
        seccomp_release(ctx);
        return ret;
    }

int main()
{
    setup_seccomp_whitelist();

    // Put your code below
    return 0;

}
