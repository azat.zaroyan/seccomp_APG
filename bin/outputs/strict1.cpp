/*
 * Generated seccomp template with initialized filter using seccomp_APG.
 * link with -lseccomp
 */

#ifdef __cplusplus
    #define NULL nullptr
    #include <cstdio>
#else
    #include <stdio.h>
#endif

#include <seccomp.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cerrno>
#include <unistd.h>
#include <asm/unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <signal.h>


    int setup_seccomp_whitelist(){
        scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
        int ret = 0;
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 5,
            SCMP_A1(SCMP_CMP_EQ, 134217728),
            SCMP_A2(SCMP_CMP_EQ, PROT_NONE),
            SCMP_A3(SCMP_CMP_MASKED_EQ, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE),
            SCMP_A4(SCMP_CMP_EQ, -1),
            SCMP_A5(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 2,
            SCMP_A1(SCMP_CMP_EQ, 135168),
            SCMP_A2(SCMP_CMP_MASKED_EQ, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 1,
            SCMP_A1(SCMP_CMP_EQ, 67108864)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_EQ, 1),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_EQ, 2),
            SCMP_A2(SCMP_CMP_EQ, -1)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(poll), 2,
            SCMP_A1(SCMP_CMP_EQ, 2),
            SCMP_A2(SCMP_CMP_EQ, 0)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(prctl), 1,
            SCMP_A0(SCMP_CMP_EQ, PR_SET_NAME)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 2,
            SCMP_A0(SCMP_CMP_EQ, 6),
            SCMP_A2(SCMP_CMP_EQ, 16)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(recvmsg), 2,
            SCMP_A0(SCMP_CMP_EQ, 5),
            SCMP_A2(SCMP_CMP_EQ, MSG_CMSG_CLOEXEC)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(sendmsg), 2,
            SCMP_A0(SCMP_CMP_EQ, 5),
            SCMP_A2(SCMP_CMP_EQ, MSG_NOSIGNAL)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(set_robust_list), 1,
            SCMP_A1(SCMP_CMP_EQ, 24)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 6),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        ret |= seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 2,
            SCMP_A0(SCMP_CMP_EQ, 7),
            SCMP_A2(SCMP_CMP_EQ, 8)
        );
        //-------------------------------------------------------------------------
        int filter_fd;
        filter_fd = open("/tmp/seccomp_filter.bpf", O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
        if (filter_fd == -1) {
             ret = -errno;
             goto out;
        }

        ret = seccomp_export_bpf(ctx, filter_fd);
        if (ret < 0) {
             close(filter_fd);
             goto out;
        }
        close(filter_fd);


        if (seccomp_load(ctx) != 0) {
            ret = 2;
            goto out;
        }

        return ret;

    out:
        seccomp_release(ctx);
        return ret;
    }

int main()
{
    setup_seccomp_whitelist();

    // Put your code below
    return 0;

}
